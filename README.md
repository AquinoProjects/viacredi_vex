

#              Viacredi Mobile App
Projeto desenvolvido como desafio pela empresa Grupo Vex, para entendimento das tecnologias que foram usadas.
Para abrir, faça um clone o projeto e na raiz execute o comando "flutter run".
Nota: Esse app foi desenvolvido para tablets, então se não executar usando dispositivo igual, não terá a responsividade proposta.
O preenchimento das informações vai para uma dashboard que também desenvolvi, se quiser dar uma olhada, acesse o repositório viacredi-dashboard.
# Tecnologias Usadas
🔥Dart
🔥Flutter
🔥Firestore
