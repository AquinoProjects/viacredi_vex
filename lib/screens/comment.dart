import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:viacredi_vex/components/background_base.dart';
import 'package:viacredi_vex/components/create_elevated_button.dart';
import 'package:viacredi_vex/models/firestore_db.dart';

class Comment extends StatefulWidget {
  @override
  _CommentState createState() => _CommentState();
}

class _CommentState extends State<Comment> {
  void reset() {
    Navigator.pushNamedAndRemoveUntil(context, "/", (r) => false);
  }

  Timer? _timer;
  TextEditingController _commentText = TextEditingController();
  @override
  void initState() {
    super.initState();
    _timer = Timer(Duration(seconds: 10), () {
      Navigator.pushNamedAndRemoveUntil(context, "/", (r) => false);
    });
  }

  bool button = false;
  List<Widget> _createListCommentButtons() {
    final List<Widget> commentButtonList = [];
    for (var i = 0; i <= 1; i++) {
      String _color = "";
      String _text = "";
      switch (i) {
        case 0:
          _color = '55a548';
          _text = 'Enviar';
          break;
        case 1:
          _color = 'f9a82f';
          _text = 'Não, obrigado';
          break;
      }
      commentButtonList.add(
        CreateElevatedButton(
            _color,
            EdgeInsets.fromLTRB(40, 10, 30, 10),
            null,
            BorderRadius.circular(46),
            Text(
              _text,
              style: TextStyle(fontSize: 35),
            ), () {
          if (_text == 'Enviar' && _commentText.text.length > 0) {
            FireStoreDb().saveValue(_commentText.text, 'comment');
            Navigator.pushNamedAndRemoveUntil(context, "/Sucess", (r) => false);
          } else if (_text == 'Não, obrigado') {
            Navigator.pushNamedAndRemoveUntil(context, "/Sucess", (r) => false);
          } else {
            Fluttertoast.showToast(
                msg: "Por favor, deixe seu comentário!",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIosWeb: 2,
                backgroundColor: Color(0xFFfd4a59),
                textColor: Color(0XFFFFFFFF),
                fontSize: 25.0);
          }
        }),
      );
    }
    return commentButtonList;
  }

  void restartTimer() {
    if (_timer != null) {
      _timer!.cancel();
    }

    _timer = new Timer(Duration(seconds: 10), () {
      reset();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          restartTimer();
        });
      },
      child: SafeArea(
        child: Scaffold(
          body: Stack(
            children: [
              backgroundBaseBg(),
              backgroundBaseDesign(),
              Container(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      backgroundBaseLogo(),
                      Padding(
                        padding: const EdgeInsets.only(right: 85),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            if (button) ...[
                              CreateElevatedButton(
                                  '66bb6a',
                                  EdgeInsets.only(
                                      left: 20, right: 20, top: 20, bottom: 20),
                                  null,
                                  BorderRadius.circular(46),
                                  Text(
                                    'Finalizar comentário',
                                    style: TextStyle(fontSize: 20),
                                  ), () {
                                FocusScope.of(context).unfocus();
                                button = false;
                                restartTimer();
                                setState(() {
                                  button = true;
                                });
                              })
                            ],
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 0),
                              child: Text(
                                'Deixe seu comentário',
                                style: TextStyle(
                                  fontSize: 50,
                                  fontFamily: 'Hind',
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 200,
                        width: 890,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(25),
                          ),
                          border: Border.all(
                            color: Color(0XFFbdbdbd),
                            width: 2,
                          ),
                        ),
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: 20, right: 20, top: 5, bottom: 5),
                          child: TextField(
                            controller: _commentText,
                            onChanged: (value) {
                              setState(() {
                                restartTimer();
                                button = true;
                              });
                            },
                            decoration:
                                InputDecoration(border: InputBorder.none),
                            style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.w400,
                              color: Color(0XFF1e1e1e),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8),
                        child: Container(
                          width: double.maxFinite,
                          child: Wrap(
                            spacing: 100,
                            alignment: WrapAlignment.center,
                            children: _createListCommentButtons(),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
