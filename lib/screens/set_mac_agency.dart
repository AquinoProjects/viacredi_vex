import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:viacredi_vex/components/background_base.dart';
import 'package:viacredi_vex/components/create_elevated_button.dart';
import 'package:viacredi_vex/models/firestore_db.dart';

class SetMacAndAgency extends StatefulWidget {
  @override
  _SetMacAndAgencyState createState() => _SetMacAndAgencyState();
}

class _SetMacAndAgencyState extends State<SetMacAndAgency> {
  TextEditingController _macText = TextEditingController();
  TextEditingController _agencyText = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: SafeArea(
        child: Scaffold(
          body: Stack(
            children: [
              backgroundBaseBg(),
              backgroundBaseDesign(),
              SingleChildScrollView(
                child: Container(
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              color: Color(0XFF158092),
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                            ),
                            child: IconButton(
                                iconSize: 50,
                                color: Color(0XFFf0f3f4),
                                onPressed: () {
                                  Navigator.pushNamedAndRemoveUntil(
                                      context, "/", (r) => false);
                                },
                                icon: Icon(Icons.home_sharp)),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 25, bottom: 65),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Configuração do Dispositivo',
                              style: TextStyle(fontSize: 35),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 40),
                            child: Text(
                              'Mac Address',
                              style: TextStyle(fontSize: 30),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 17),
                            child: Text(
                              'Agência',
                              style: TextStyle(fontSize: 30),
                            ),
                          ),
                        ],
                      ),
                      SingleChildScrollView(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height: 50,
                              width: 220,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                border: Border.all(
                                  color: Color(0XFFbdbdbd),
                                  width: 2,
                                ),
                              ),
                              child: Padding(
                                padding: EdgeInsets.only(
                                    left: 20, right: 20, top: 5, bottom: 5),
                                child: TextField(
                                  controller: _macText,
                                  decoration:
                                      InputDecoration(border: InputBorder.none),
                                  style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0XFF1e1e1e),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              height: 50,
                              width: 180,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                border: Border.all(
                                  color: Color(0XFFbdbdbd),
                                  width: 2,
                                ),
                              ),
                              child: Padding(
                                padding: EdgeInsets.only(
                                    left: 20, right: 20, top: 5, bottom: 5),
                                child: TextField(
                                  keyboardType:
                                      TextInputType.numberWithOptions(),
                                  controller: _agencyText,
                                  decoration:
                                      InputDecoration(border: InputBorder.none),
                                  style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0XFF1e1e1e),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 70, left: 40),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CreateElevatedButton(
                                '0277bd',
                                EdgeInsets.fromLTRB(75, 20, 75, 15),
                                null,
                                BorderRadius.circular(28),
                                Text(
                                  'Salvar',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 35),
                                ), () {
                              if (_macText.text.length > 0 &&
                                  _agencyText.text.length > 0) {
                                FireStoreDb().saveDevicesMac(_macText.text);
                                FireStoreDb().saveDevicesAgency(
                                    int.parse(_agencyText.text));
                                _macText.text = "";
                                _agencyText.text = "";
                                Fluttertoast.showToast(
                                    msg: "Dados atualizados",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.TOP,
                                    timeInSecForIosWeb: 2,
                                    backgroundColor: Color(0XFF40b42a),
                                    textColor: Colors.white,
                                    fontSize: 25.0);
                                Timer(Duration(seconds: 2), () {
                                  Navigator.pushNamedAndRemoveUntil(
                                      context, "/", (r) => false);
                                });
                              } else {
                                Fluttertoast.showToast(
                                    msg: "Preencha os dados corretamente",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.TOP,
                                    timeInSecForIosWeb: 2,
                                    backgroundColor: Color(0XFFfd4a59),
                                    textColor: Colors.white,
                                    fontSize: 25.0);
                              }
                            }),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
