import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:viacredi_vex/components/background_base.dart';
import 'package:viacredi_vex/components/create_elevated_button.dart';
import 'package:cpf_cnpj_validator/cpf_validator.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:viacredi_vex/models/firestore_db.dart';

class InsertCpf extends StatefulWidget {
  @override
  _InsertCpfState createState() => _InsertCpfState();
}

class _InsertCpfState extends State<InsertCpf> {
  void reset() {
    Navigator.pushNamedAndRemoveUntil(context, "/", (r) => false);
  }

  Timer? _timer;
  @override
  void initState() {
    super.initState();
    _timer = Timer(Duration(seconds: 10), () {
      Navigator.pushNamedAndRemoveUntil(context, "/", (r) => false);
    });
  }

  String _textCpf = '';
  List<Widget> _createListCpfNumericButtons() {
    final List<Widget> buttonsList = [];
    for (var i = 1; i < 13; i++) {
      if (i == 10) {
        buttonsList.add(Container());
      } else if (i == 11) {
        buttonsList.add(
          CreateElevatedButton(
            '0277bd',
            EdgeInsets.all(10),
            null,
            BorderRadius.circular(18),
            Text(
              '0',
              style: TextStyle(fontSize: 30),
            ),
            () {
              setState(() {
                restartTimer();
                if (_textCpf.length >= 0 && _textCpf.length < 11) {
                  _textCpf += '0';
                }
              });
            },
          ),
        );
      } else if (i == 12) {
        buttonsList.add(
          CreateElevatedButton(
            '0277bd',
            EdgeInsets.all(10),
            null,
            BorderRadius.circular(18),
            Icon(Icons.backspace_outlined),
            () {
              setState(() {
                restartTimer();
                if (_textCpf.length >= 0) {
                  _textCpf = _textCpf.substring(0, _textCpf.length - 1);
                }
              });
            },
          ),
        );
      } else {
        buttonsList.add(
          CreateElevatedButton(
            '0277bd',
            EdgeInsets.all(10),
            null,
            BorderRadius.circular(18),
            Text(
              i.toString(),
              style: TextStyle(fontSize: 30),
            ),
            () {
              restartTimer();
              setState(() {
                if (_textCpf.length >= 0 && _textCpf.length < 11) {
                  _textCpf += "${i.toString()}";
                }
              });
            },
          ),
        );
      }
    }

    return buttonsList;
  }

  void restartTimer() {
    if (_timer != null) {
      _timer!.cancel();
    }

    _timer = new Timer(Duration(seconds: 10), () {
      reset();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          restartTimer();
        });
      },
      child: SafeArea(
        child: Scaffold(
          body: Stack(
            children: [
              backgroundBaseBg(),
              backgroundBaseDesign(),
              Container(
                child: Column(
                  children: [
                    backgroundBaseLogo(),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            height: 50,
                            width: 448,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(25),
                                topRight: Radius.circular(25),
                              ),
                              border: Border.all(
                                color: Color(0XFF0277bd),
                                width: 4,
                              ),
                            ),
                            child: Center(
                              child: Text(
                                CPFValidator.format(_textCpf),
                                style: TextStyle(
                                  fontSize: 35,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0XFF0277bd),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            height: 260,
                            width: 448,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(25),
                                bottomRight: Radius.circular(25),
                              ),
                              border: Border.all(
                                color: Color(0XFF0277bd),
                                width: 4,
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 20, right: 20, top: 5, bottom: 5),
                              child: GridView.count(
                                crossAxisCount: 3,
                                mainAxisSpacing: 10,
                                crossAxisSpacing: 10,
                                childAspectRatio: 2.5,
                                padding: EdgeInsets.all(5),
                                children: _createListCpfNumericButtons(),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 9),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CreateElevatedButton(
                            'f9a82f',
                            EdgeInsets.fromLTRB(55, 12, 55, 12),
                            null,
                            BorderRadius.circular(36),
                            Text(
                              'Enviar',
                              style: TextStyle(fontSize: 40),
                            ),
                            () {
                              setState(() {
                                if (_textCpf.length != 11) {
                                  Fluttertoast.showToast(
                                      msg: "Por favor, digite um CPF válido!",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 2,
                                      backgroundColor: Color(0xFFfd4a59),
                                      textColor: Color(0XFFffffff),
                                      fontSize: 25.0);
                                  restartTimer();
                                } else {
                                  FireStoreDb().saveValue(_textCpf, 'cpf');
                                  Navigator.pushNamedAndRemoveUntil(
                                      context, "/Comment", (r) => false);
                                }
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
