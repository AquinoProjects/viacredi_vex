import 'dart:async';

import 'package:flutter/material.dart';
import 'package:viacredi_vex/components/background_base.dart';

class Sucess extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Timer(Duration(seconds: 3), () {
      Navigator.pushNamedAndRemoveUntil(context, "/", (r) => false);
    });

    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            backgroundBaseBg(),
            backgroundBaseDesign(),
            Container(
              child: Column(
                children: [
                  backgroundBaseLogo(),
                  Padding(
                    padding: const EdgeInsets.only(top: 50),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Obrigado!',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 110,
                            color: Color(0XFF757575),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      RichText(
                        text: TextSpan(
                          style: TextStyle(
                            fontSize: 45,
                            color: Color(0XFF545556),
                          ),
                          children: <TextSpan>[
                            TextSpan(text: 'Seus dados foram enviados com '),
                            TextSpan(
                                text: 'sucesso!',
                                style: TextStyle(color: Color(0XFFfac56e))),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
