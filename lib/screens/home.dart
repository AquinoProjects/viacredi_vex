import 'dart:async';
import 'package:flutter/material.dart';
import 'package:viacredi_vex/components/background_base.dart';
import 'package:viacredi_vex/components/create_elevated_button.dart';
import 'package:viacredi_vex/models/firestore_db.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  void reset() {
    setState(() {
      _updateIndex = -1;
    });
  }

  int _doubleTap = 0;
  Timer? _timer;
  @override
  void initState() {
    super.initState();
  }

  int _updateIndex = -1;
  List<Widget> _createListButtons() {
    final List<Widget> buttonsList = [];
    for (var i = 0; i < 11; i++) {
      String _color = "";
      switch (i) {
        case 0:
          _color = '5b1f16';
          break;
        case 1:
          _color = '942a18';
          break;
        case 2:
          _color = 'e74529';
          break;
        case 3:
          _color = 'e73c22';
          break;
        case 4:
          _color = 'ec681c';
          break;
        case 5:
          _color = 'f39019';
          break;
        case 6:
          _color = 'fbba18';
          break;
        case 7:
          _color = 'fcda23';
          break;
        case 8:
          _color = 'cccc5f';
          break;
        case 9:
          _color = '69b436';
          break;
        case 10:
          _color = '316e2e';
          break;
        case 11:
          _color = '5b1f16';
          break;
      }

      buttonsList.add(
        CreateElevatedButton(
          _updateIndex != i ? _color : '1fa57a',
          EdgeInsets.fromLTRB(20, 10, 20, 10),
          _updateIndex != i ? EdgeInsets.all(0) : EdgeInsets.only(top: 20),
          BorderRadius.circular(18),
          Text(
            i.toString(),
            style: TextStyle(fontSize: 55),
          ),
          () {
            restartTimer();

            setState(() {
              _updateIndex = i;
            });
          },
        ),
      );
    }

    return buttonsList;
  }

  void restartTimer() {
    if (_timer != null) {
      _timer!.cancel();
    }

    _timer = new Timer(Duration(seconds: 10), () {
      reset();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: GestureDetector(
          onTap: () {
            restartTimer();
          },
          child: SingleChildScrollView(
            child: Stack(
              children: [
                backgroundBaseBg(),
                backgroundBaseDesign(),
                Container(
                  child: Column(
                    children: [
                      GestureDetector(
                          onTap: () {
                            _doubleTap++;
                            if (_doubleTap > 9) {
                              Navigator.pushNamedAndRemoveUntil(
                                  context, "/SetMacAndAgency", (r) => false);
                            }
                          },
                          child: backgroundBaseLogo()),
                      Padding(
                        padding: const EdgeInsets.only(top: 30),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Em uma escala de 0 a 10 o quanto você indicaria a\nexperiência de hoje para amigos e familiares?',
                              style: TextStyle(
                                fontSize: 40,
                                fontFamily: 'Hind',
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(30),
                        child: Container(
                          width: double.maxFinite,
                          child: Wrap(
                            alignment: WrapAlignment.spaceBetween,
                            children: _createListButtons(),
                          ),
                        ),
                      ),
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CreateElevatedButton(
                                  '0277bd',
                                  EdgeInsets.fromLTRB(30, 15, 30, 15),
                                  null,
                                  BorderRadius.circular(36),
                                  Text(
                                    'Enviar',
                                    style: TextStyle(fontSize: 45),
                                  ),
                                  (_updateIndex != -1)
                                      ? () {
                                          FireStoreDb().saveValue(
                                              _updateIndex, 'search-note');
                                          Navigator.pushNamedAndRemoveUntil(
                                              context,
                                              "/GeneralSearch",
                                              (r) => false);
                                        }
                                      : null,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
