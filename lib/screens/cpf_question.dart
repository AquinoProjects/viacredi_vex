import 'dart:async';

import 'package:flutter/material.dart';
import 'package:viacredi_vex/components/background_base.dart';
import 'package:viacredi_vex/components/create_elevated_button.dart';

class CpfQuestion extends StatefulWidget {
  @override
  _CpfQuestionState createState() => _CpfQuestionState();
}

class _CpfQuestionState extends State<CpfQuestion> {
  void reset() {
    Navigator.pushNamedAndRemoveUntil(context, "/", (r) => false);
  }

  Timer? _timer;
  @override
  void initState() {
    super.initState();
    _timer = Timer(Duration(seconds: 10), () {
      Navigator.pushNamedAndRemoveUntil(context, "/", (r) => false);
    });
  }

  List<Widget> _createListCpfButtons() {
    final List<Widget> cpfButtonList = [];
    for (var i = 0; i <= 1; i++) {
      String _color = "";
      String _text = "";
      switch (i) {
        case 0:
          _color = '55a548';
          _text = 'Sim';
          break;
        case 1:
          _color = 'f9a82f';
          _text = 'Não';
          break;
      }
      cpfButtonList.add(
        CreateElevatedButton(
            _color,
            EdgeInsets.fromLTRB(40, 20, 30, 20),
            null,
            BorderRadius.circular(46),
            Text(
              _text,
              style: TextStyle(fontSize: 55),
            ), () {
          if (i == 0) {
            Navigator.pushNamedAndRemoveUntil(
                context, "/InsertCpf", (r) => false);
          } else {
            Navigator.pushNamedAndRemoveUntil(context, "/Sucess", (r) => false);
          }
        }),
      );
    }
    return cpfButtonList;
  }

  void restartTimer() {
    if (_timer != null) {
      _timer!.cancel();
    }

    _timer = new Timer(Duration(seconds: 10), () {
      reset();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        restartTimer();
      },
      child: SafeArea(
        child: Scaffold(
          body: Stack(
            children: [
              backgroundBaseBg(),
              backgroundBaseDesign(),
              Container(
                child: Column(
                  children: [
                    backgroundBaseLogo(),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Gostaria de informar seu CPF?',
                            style: TextStyle(
                              fontSize: 55,
                              fontFamily: 'Hind',
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 40),
                      child: Container(
                        width: double.maxFinite,
                        child: Wrap(
                          spacing: 100,
                          alignment: WrapAlignment.center,
                          children: _createListCpfButtons(),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
