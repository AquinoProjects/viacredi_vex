import 'dart:async';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:viacredi_vex/components/background_base.dart';
import 'package:viacredi_vex/components/create_elevated_button.dart';
import 'package:viacredi_vex/models/firestore_db.dart';

class GeneralSearch extends StatefulWidget {
  @override
  _GeneralSearchState createState() => _GeneralSearchState();
}

class _GeneralSearchState extends State<GeneralSearch> {
  Timer? _timer;
  @override
  void initState() {
    super.initState();
    _timer = Timer(Duration(seconds: 10), () {
      Navigator.pushNamedAndRemoveUntil(context, "/", (r) => false);
    });
  }

  int _updateIndex = -1;
  int _secondUpdateIndex = -1;
  int _thirdUpdateIndex = -1;

  List<IconButton> _createListStarsButtons() {
    final List<IconButton> starsButtonList = [];
    for (var i = 1; i <= 5; i++) {
      Image _defaultStar = Image.asset('images/starlight.png');

      if (i <= _updateIndex)
        _defaultStar = Image.asset('images/estrela_active.png');

      starsButtonList.add(
        IconButton(
          padding: EdgeInsets.only(right: 5, left: 5),
          onPressed: () {
            restartTimer();
            setState(() {
              _updateIndex = i;
            });
          },
          icon: _defaultStar,
          iconSize: 90,
        ),
      );
    }

    return starsButtonList;
  }

  List<IconButton> _createSecondListStarsButtons() {
    final List<IconButton> starsButtonList = [];
    for (var i = 1; i <= 5; i++) {
      Image _defaultStar = Image.asset('images/starlight.png');

      if (i <= _secondUpdateIndex)
        _defaultStar = Image.asset('images/estrela_active.png');

      starsButtonList.add(
        IconButton(
          padding: EdgeInsets.only(right: 5, left: 5),
          onPressed: () {
            restartTimer();
            setState(() {
              _secondUpdateIndex = i;
            });
          },
          icon: _defaultStar,
          iconSize: 90,
        ),
      );
    }

    return starsButtonList;
  }

  List<IconButton> _createThirdListStarsButtons() {
    final List<IconButton> starsButtonList = [];
    for (var i = 1; i <= 5; i++) {
      Image _defaultStar = Image.asset('images/starlight.png');

      if (i <= _thirdUpdateIndex)
        _defaultStar = Image.asset('images/estrela_active.png');

      starsButtonList.add(
        IconButton(
          padding: EdgeInsets.only(right: 5, left: 5),
          onPressed: () {
            restartTimer();
            setState(() {
              _thirdUpdateIndex = i;
            });
          },
          icon: _defaultStar,
          iconSize: 90,
        ),
      );
    }

    return starsButtonList;
  }

  void reset() {
    Navigator.pushNamedAndRemoveUntil(context, "/", (r) => false);
    if (_updateIndex != -1)
      FireStoreDb().saveValue(_updateIndex, 'general-search-environment');
    if (_secondUpdateIndex != -1)
      FireStoreDb().saveValue(_secondUpdateIndex, 'general-search-employee');
    if (_thirdUpdateIndex != -1)
      FireStoreDb().saveValue(_thirdUpdateIndex, 'general-search-time');
  }

  void restartTimer() {
    if (_timer != null) {
      _timer!.cancel();
    }

    _timer = new Timer(Duration(seconds: 10), () {
      reset();
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        restartTimer();
      },
      child: SafeArea(
        child: Scaffold(
          body: Stack(
            children: [
              backgroundBaseBg(),
              backgroundBaseDesign(),
              Container(
                child: Column(
                  children: [
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Ambiente do Posto de Atendimento',
                            style: TextStyle(
                              fontSize: 36,
                              fontFamily: 'Hind',
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Wrap(
                        alignment: WrapAlignment.center,
                        children: _createListStarsButtons(),
                      ),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Atendimento dos colaboradores',
                            style: TextStyle(
                              fontSize: 36,
                              fontFamily: 'Hind',
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Wrap(
                        alignment: WrapAlignment.center,
                        children: _createSecondListStarsButtons(),
                      ),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Tempo de espera',
                            style: TextStyle(
                              fontSize: 36,
                              fontFamily: 'Hind',
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Wrap(
                        alignment: WrapAlignment.center,
                        children: _createThirdListStarsButtons(),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 30),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CreateElevatedButton(
                            'f9a82f',
                            EdgeInsets.fromLTRB(55, 12, 55, 12),
                            null,
                            BorderRadius.circular(36),
                            Text(
                              'Enviar',
                              style: TextStyle(fontSize: 40),
                            ),
                            () {
                              if (_updateIndex != -1 &&
                                  _secondUpdateIndex != -1 &&
                                  _thirdUpdateIndex != -1) {
                                FireStoreDb().saveValue(
                                    _updateIndex, 'general-search-environment');
                                FireStoreDb().saveValue(_secondUpdateIndex,
                                    'general-search-employee');
                                FireStoreDb().saveValue(
                                    _thirdUpdateIndex, 'general-search-time');

                                Navigator.pushNamedAndRemoveUntil(
                                    context, "/CpfQuestion", (r) => false);
                              } else {
                                restartTimer();
                                Fluttertoast.showToast(
                                    msg:
                                        "Por favor, avalie todos os campos! :)",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.BOTTOM,
                                    timeInSecForIosWeb: 2,
                                    backgroundColor: Color(0xFFfd4a59),
                                    textColor: Colors.white,
                                    fontSize: 25.0);
                              }
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
