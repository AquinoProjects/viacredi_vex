import 'package:flutter/material.dart';
//classe para criação dinâmica do ElevatedButton atendendo as demandas do projeto.
class CreateElevatedButton extends StatelessWidget {
  final String _setStringColor;
  final EdgeInsets _edgeInsets;
  final EdgeInsets? _insetsMargin;
  final BorderRadius _borderRadius;
  final Widget _childButton;
  final Function()? functionButton;

  CreateElevatedButton(
      this._setStringColor,
      this._edgeInsets,
      this._insetsMargin,
      this._borderRadius,
      this._childButton,
      this.functionButton);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: _insetsMargin,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: setColor(_setStringColor),
          shape: RoundedRectangleBorder(
            borderRadius: _borderRadius,
          ),
          padding: _edgeInsets,
        ),
        onPressed: functionButton,
        child: _childButton,
      ),
    );
  }

  Color setColor(String _stringColor) {
    final String _baseColorMaterial = '0xFF';
    return Color(
      int.parse(
        (_baseColorMaterial + _stringColor),
      ),
    );
  }
}
