import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:jiffy/jiffy.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FireStoreDb {
  FireStoreDb._privateConstructor();
  final CollectionReference _search =
      FirebaseFirestore.instance.collection('search');
  DocumentReference? _doc;

  static final FireStoreDb _instance = FireStoreDb._privateConstructor();

  factory FireStoreDb() {
    return _instance;
  }

  Future<bool> saveDevicesMac(String macReceived) async {
    SharedPreferences mac = await SharedPreferences.getInstance();
    await mac.setString("mac", macReceived);
    return true;
  }

  Future<bool> saveDevicesAgency(int agencyReceived) async {
    SharedPreferences agency = await SharedPreferences.getInstance();
    await agency.setInt("agency", agencyReceived);
    return true;
  }

  Future<String> getDevicesMac() async {
    SharedPreferences mac = await SharedPreferences.getInstance();
    String? getMac = mac.getString("mac");
    return getMac!;
  }

  Future<int> getDevicesAgency() async {
    SharedPreferences agency = await SharedPreferences.getInstance();
    int? getAgency = agency.getInt("agency");
    return getAgency!;
  }
  //função para salvar os dados no banco.
  //Quando existir no campo o valor 'search-note' os dados abaixo da validação serão iniciados, conforme está sendo persistido no banco. 
  saveValue(dynamic valor, String campo) async {
    if (campo == 'search-note') {
      String mac = await getDevicesMac();
      int agency = await getDevicesAgency();
      _doc = await _search.add({
        campo: valor,
        "mac": mac,
        "agency": agency,
        "hour": int.parse(Jiffy(DateTime.now()).format('hhmm')),
        "date": int.parse(
          Jiffy(DateTime.now()).format("yyyyMMdd"),
        ),
      });
    } else if (_doc != null) {
      _search.doc(_doc!.id).update({campo: valor});
    }
  }
}
