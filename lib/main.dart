import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:viacredi_vex/screens/comment.dart';
import 'package:viacredi_vex/screens/cpf_question.dart';
import 'package:viacredi_vex/screens/general_search.dart';
import 'package:viacredi_vex/screens/home.dart';
import 'package:viacredi_vex/screens/insert_cpf.dart';
import 'package:viacredi_vex/screens/set_mac_agency.dart';
import 'package:viacredi_vex/screens/sucess.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(Viacredi());
}

class Viacredi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);
    return MaterialApp(
      routes: <String, WidgetBuilder>{
        '/': (BuildContext context) => Home(),
        '/GeneralSearch': (BuildContext context) => GeneralSearch(),
        '/CpfQuestion': (BuildContext context) => CpfQuestion(),
        '/InsertCpf': (BuildContext context) => InsertCpf(),
        '/Comment': (BuildContext context) => Comment(),
        '/Sucess': (BuildContext context) => Sucess(),
        '/SetMacAndAgency': (BuildContext context) => SetMacAndAgency(),
      },
      theme: ThemeData(
        textTheme: GoogleFonts.balsamiqSansTextTheme(),
      ),
      debugShowCheckedModeBanner: false,
      title: 'Viacredi',
      initialRoute: '/',
    );
  }
}
